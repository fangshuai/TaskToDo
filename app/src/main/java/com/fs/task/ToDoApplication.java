package com.fs.task;

import android.app.Application;

import com.fs.task.component.ApplicationModule;
import com.fs.task.component.DaggerTasksRepositoryComponent;
import com.fs.task.component.TasksRepositoryComponent;

/**
 * Created by fangshuai on 2016/4/13.
 */
public class ToDoApplication extends Application {

    private static ToDoApplication instance;

    private TasksRepositoryComponent mRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mRepositoryComponent = DaggerTasksRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
    }

    public TasksRepositoryComponent getTasksRepositoryComponent() {
        return mRepositoryComponent;
    }

    public static ToDoApplication getInstance() {
        return instance;
    }
}
