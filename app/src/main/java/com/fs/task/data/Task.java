package com.fs.task.data;

import java.util.UUID;

/**
 * Created by fangshuai on 2016/4/13.
 */
public class Task {
    private final String mId;
    private final String mTitle;
    private final String mDescription;
    private final boolean mComplete;

    public Task(String mTitle, String mDescription) {
        mId = UUID.randomUUID().toString();
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        mComplete = false;
    }

    public Task(String mTitle, String mDescription, String mId) {
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mId = mId;
        mComplete = false;
    }

    public Task(String mTitle, String mDescription, boolean mComplete) {
        mId = UUID.randomUUID().toString();
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mComplete = mComplete;
    }

    public Task(String mTitle, String mDescription, String mId, boolean mComplete) {
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mId = mId;
        this.mComplete = mComplete;
    }

    public boolean isComplete() {
        return mComplete;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getTitleForList() {
        if (mTitle != null && !mTitle.equals("")) {
            return mTitle;
        } else {
            return mDescription;
        }
    }

    public boolean isActive() {
        return !mComplete;
    }

    public boolean isEmpty() {
        return (mTitle == null || "".equals(mTitle)) && (mDescription == null || "".equals(mDescription));
    }

    @Override
    public String toString() {
        return "Task with title " + mTitle;
    }
}
