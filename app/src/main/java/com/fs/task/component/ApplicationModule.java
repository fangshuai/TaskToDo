package com.fs.task.component;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by fangshuai on 2016/4/13.
 */
@Module
public class ApplicationModule {
    private final Context mContext;

    public ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    public Context provideContext() {
        return mContext;
    }
}
