package com.fs.task.component;

import android.content.Context;

import com.fs.task.data.source.Local;
import com.fs.task.data.source.TasksDataSource;
import com.fs.task.data.source.local.TasksLocalDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by fangshuai on 2016/4/13.
 */
@Module
public class TasksRepositoryModule {

    @Singleton
    @Provides
    @Local
    TasksDataSource provideTasksLocalDataSource(Context context) {
        return new TasksLocalDataSource(context);
    }
}
