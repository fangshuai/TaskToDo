package com.fs.task.component;

import com.fs.task.presenter.TasksPresenter;
import com.fs.task.util.FragmentScoped;

import dagger.Component;

/**
 * Created by fangshuai on 2016/4/13.
 */
@FragmentScoped
@Component(dependencies = TasksRepositoryComponent.class, modules = TasksPresenterModule.class)
public interface TasksFragmentComponent {
    TasksPresenter getTaskPresenter();
}
