package com.fs.task.component;

import com.fs.task.ui.contract.TasksContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by fangshuai on 2016/4/13.
 */
@Module
public class TasksPresenterModule {
    private final TasksContract.View mView;

    public TasksPresenterModule(TasksContract.View view) {
        mView = view;
    }

    @Provides
    TasksContract.View provideTasksContractView() {
        return mView;
    }
}
