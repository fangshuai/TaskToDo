package com.fs.task.component;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by fangshuai on 2016/4/13.
 */
@Singleton
@Component(modules = { TasksRepositoryModule.class, ApplicationModule.class })
public interface TasksRepositoryComponent {

    TasksRepositoryModule getTasksRepository();

}
