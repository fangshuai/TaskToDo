package com.fs.task.ui.contract;

import com.fs.task.data.Task;

import java.util.List;

/**
 * Created by fangshuai on 2016/4/13.
 */
public interface TasksContract {
    interface View {
        void setProgressIndicator(boolean active);

        void showTasks(List<Task> tasks);

        void showAddTask();

        void showTaskDetailsUi(String taskId);

        void showTaskMarkedComplete();

        void showTaskMarkedActive();

        void showCompletedTasksCleared();

        void showLoadingTasksError();

        boolean isInactive();
    }

    interface UserActionsListener {
        void addNewTask();

        void loadAllTasks(boolean forceUpdate);

        void loadActiveTasks(boolean forceUpdate);

        void loadCompletedTasks(boolean forceUpdate);

        void openTaskDetails(Task requestedTask);

        void completeTask(Task completedTask);

        void activateTask(Task activeTask);

        void clearCompletedTasks();
    }
}
