package com.fs.task.presenter;

import com.fs.task.data.Task;
import com.fs.task.data.source.TasksRepository;
import com.fs.task.ui.contract.TasksContract;

import javax.inject.Inject;

import static com.fs.task.ui.TasksFragment.ACTIVE_TASKS;
import static com.fs.task.ui.TasksFragment.ALL_TASKS;
import static com.fs.task.ui.TasksFragment.COMPLETED_TASKS;

/**
 * Created by fangshuai on 2016/4/13.
 */
public class TasksPresenter implements TasksContract.UserActionsListener {

    private final TasksRepository mTasksRepository;

    private final TasksContract.View mTasksView;

    @Inject
    public TasksPresenter(TasksRepository tasksRepository, TasksContract.View tasksView) {
        mTasksRepository = tasksRepository;
        mTasksView = tasksView;
    }

    @Override
    public void addNewTask() {
        mTasksView.showAddTask();
    }

    @Override
    public void loadAllTasks(boolean forceUpdate) {
        loadTasks(forceUpdate, true, ALL_TASKS);
    }



    @Override
    public void loadActiveTasks(boolean forceUpdate) {

    }

    @Override
    public void loadCompletedTasks(boolean forceUpdate) {

    }

    private void loadTasks(boolean forceUpdate, final boolean showLoadingUI, final int requestType) {
        if (showLoadingUI) {
            mTasksView.setProgressIndicator(true);
        }
        //TODO
    }

    @Override
    public void openTaskDetails(Task requestedTask) {

    }

    @Override
    public void completeTask(Task completedTask) {

    }

    @Override
    public void activateTask(Task activeTask) {

    }

    @Override
    public void clearCompletedTasks() {

    }
}
